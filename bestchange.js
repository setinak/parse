require('dotenv').config()

const cheerio = require('cheerio');
const got = require('got');
const { google } = require('googleapis');
const fs = require('fs');
const readline = require('readline');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'token.json';

const sites = [
    { direction: 'BTC - Сбербанк', url: 'https://www.bestchange.ru/bitcoin-to-sberbank.html' },
    { direction: 'ETH - Сбербанк', url: 'https://www.bestchange.ru/ethereum-to-sberbank.html' },
    { direction: 'Tether Omni (USDT) - Сбербанк', url: 'https://www.bestchange.ru/tether-to-sberbank.html' },
    { direction: 'Tether ERC20 (USDT) - Сбербанк', url: 'https://www.bestchange.ru/tether-erc20-to-sberbank.html' },

    { direction: 'BTC - Тинькофф', url: 'https://www.bestchange.ru/bitcoin-to-tinkoff.html' },
    { direction: 'ETH - Тинькофф', url: 'https://www.bestchange.ru/ethereum-to-tinkoff.html' },
    { direction: 'Tether Omni (USDT) - Тинькофф', url: 'https://www.bestchange.ru/tether-to-tinkoff.html' },
    { direction: 'Tether ERC20 (USDT) - Тинькофф', url: 'https://www.bestchange.ru/tether-erc20-to-tinkoff.html' },

    { direction: 'BTC - Альфа cash in', url: 'https://www.bestchange.ru/bitcoin-to-alfabank-cash-in.html' },
    { direction: 'ETH - Альфа cash in', url: 'https://www.bestchange.ru/ethereum-to-alfabank-cash-in.html' },
    { direction: 'Tether Omni (USDT) - Альфа cash in', url: 'https://www.bestchange.ru/tether-to-alfabank-cash-in.html' },
    { direction: 'Tether ERC20 (USDT) - Альфа cash in', url: 'https://www.bestchange.ru/tether-erc20-to-alfabank-cash-in.html' },

    { direction: 'BTC - Альфа-банк', url: 'https://www.bestchange.ru/bitcoin-to-alfaclick.html' },
    { direction: 'ETH - Альфа-банк', url: 'https://www.bestchange.ru/ethereum-to-alfaclick.html' },
    { direction: 'Tether Omni (USDT) - Альфа-банк', url: 'https://www.bestchange.ru/tether-to-alfaclick.html' },
    { direction: 'Tether ERC20 (USDT) - Альфа-банк', url: 'https://www.bestchange.ru/tether-erc20-to-alfaclick.html' },

    { direction: 'BTC - ВТБ', url: 'https://www.bestchange.ru/bitcoin-to-vtb.html' },
    { direction: 'ETH - ВТБ', url: 'https://www.bestchange.ru/ethereum-to-vtb.html' },
    { direction: 'Tether Omni (USDT) - ВТБ', url: 'https://www.bestchange.ru/tether-to-vtb.html' },
    { direction: 'Tether ERC20 (USDT) - ВТБ', url: 'https://www.bestchange.ru/tether-erc20-to-vtb.html' },

    { direction: 'BTC - Газпромбанк', url: 'https://www.bestchange.ru/bitcoin-to-gazprombank.html' },
    { direction: 'ETH - Газпромбанк', url: 'https://www.bestchange.ru/ethereum-to-gazprombank.html' },
    { direction: 'Tether Omni (USDT) - Газпромбанк', url: 'https://www.bestchange.ru/tether-to-gazprombank.html' },
    { direction: 'Tether ERC20 (USDT) - Газпромбанк', url: 'https://www.bestchange.ru/tether-erc20-to-gazprombank.html' },
];

const content = {
    installed: {
        client_id: process.env.CONTENT_CLIENT_ID,
        project_id: process.env.CONTENT_PROJECT_ID,
        auth_uri: process.env.CONTENT_AUTH_URI,
        token_uri: process.env.CONTENT_TOKEN_URI,
        auth_provider_x509_cert_url: process.env.CONTENT_AUTH_PROVIDER,
        client_secret: process.env.CONTENT_CLIENT_SECRET,
        redirect_uris: process.env.CONTENT_REDIRECT_URIS
    }
};

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

sendDataToGoogleSheet = async (auth) => {
    for (let j = 0; j < 1000; j++) {
        const dataForSend = [];

        for (let i = 0; i < sites.length; i++) {

            const result = await got(sites[i].url);

            const data = getData(result.body, 'table#content_table > tbody > tr', 'td.bj > div.pa > div.pc > div.ca', sites[i].direction)[0];
            console.log(data);
            if (data === undefined) continue;
            const { direction, rate, place } = data;
            dataForSend.push([direction, rate, place]);

            sleep(7 * 1000);
        }

        const sheets = google.sheets({version: 'v4', auth});

        sheets.spreadsheets.values.update({
            spreadsheetId: process.env.GOOGLE_SPREADSHEET_ID,
            range: 'Тестовый лист!F3:H',
            valueInputOption: 'RAW',
            resource: {
                values: dataForSend,
            }
        }, (err, response) => {
            if (err) return console.error(err)
        });

        if (j == 999) {
            j = 0;
        }
    }
}

let getData = (html, rows, exchanger, direction) => {
    const data = [];
    const $ = cheerio.load(html);
    $(rows).each((i, elem) => {        
        if ($(`${ rows } > ${ exchanger }`)[i] !== undefined) {
            const exchangerName = $(`${ rows } > ${ exchanger }`)[i].children[0].data;
            if (exchangerName === process.env.NAME) {
                const rate = $(`${ rows } > td.bi`)[1].children[0].data;
                data.push({
                    direction,
                    rate,
                    place: `${ i + 1 }`,
                });
            }
        }
    });
    return data;
}

authorize(content, sendDataToGoogleSheet);

  // Authorize a client with credentials, then call the Google Sheets API.
/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error('Error while trying to retrieve access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
            if (err) return console.error(err);
            console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
        });
    });
}